/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));


function checker(number, successCallback, errorCallback) {
        if (number>=successCallback) {
            console.log("Success!");
        } else if (number<=errorCallback) {
            console.log("Error!");
        }    
}

console.log(checker(40,19,18));

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});